# Projeto WMorais

## Instruções para executar o projeto

__Obrigatório__
- PHP >= 7.0
- node v10.15.3
- npm 6.4.1
- gulp CLI version 2.0.1
- MySQL 5.7.21

### Iniciando o projeto

__Importante:__
Antes de qualquer coisa, deve-se alterar o arquivo "wp-config.php" na raiz do projeto. Configurar a conexão com o banco de dados passando nos parâmetros indicados no arquivo. __Não Alterar a variável:__ "*$table_prefix  = 'wm_';*" desse arquivo.

#### Node
Na raiz do projeto, abra o terminal e execute "npm install" para instalar todas as dependências do projeto.

#### Gulp
O caminho pelo qual o Gulp vai minificar os arquivos é de "*/wmorais/wp-content/themes/wmorais/assets/dev*" para "*/wmorais/wp-content/themes/wmorais/assets/dist*"

Os comandos para executar a minificação dos arquivos são:

- gulp: executa a task "default" que a cada vez que um arquivo dentro da pasta "*/dev*" é modificado, a minificação ocorre de forma automática

- gulp build: minifica todos os arquivos uma única vez.

- gulp css: minifica os arquivos .css e .sass

- gulp js: minifica os arquivos .js

- gulp images: minifica as imagens que estão na pasta "*/dev/img*"

## Acessos

### Painel
Para acesso ao painel é necessário inputar a url principal + "*/wp-admin*" ao final (urlprincipal/wp-admin)

__Usuário de Desenvolvedor Administrador__

wmorais

__Senha__

W/Morais@2019

__Usuário Comum:__

wmorais-editor


__Senha:__

WMorais@Editor2019


__Obs.:__ No módulo “Opções do Tema” é onde se altera os links e nomes das redes sociais e também os
numeros e endereço de contato.

## FTP

__Usuário:__

umnovosite01

__Senha:__

WMorais2019